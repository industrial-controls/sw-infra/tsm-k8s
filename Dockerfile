FROM cern/cc7-base

MAINTAINER "Brice Copy" <brice.copy@cern.ch>

ARG TSM_DOWNLOAD_URL=https://cern.ch/project-maven/tools/TSM
ARG TSM_API_PACKAGE=TIVsm-API64.7.1.4-1.x86_64.rpm
ARG TSM_BA_PACKAGE=TIVsm-BA.7.1.4-1.x86_64.rpm
ARG GSK_CRYPT_PACKAGE=gskcrypt64.8.0-50.52.x86_64.rpm
ARG GSK_SSL_PACKAGE=gskssl64.8.0-50.52.x86_64.rpm

RUN yum -y install epel-release  && yum -y install wget compat-libstdc++-33

RUN wget -O /tmp/${TSM_API_PACKAGE} ${TSM_DOWNLOAD_URL}/${TSM_API_PACKAGE}     && \
    wget -O /tmp/${TSM_BA_PACKAGE}  ${TSM_DOWNLOAD_URL}/${TSM_BA_PACKAGE}      && \ 
    wget -O /tmp/${GSK_CRYPT_PACKAGE} ${TSM_DOWNLOAD_URL}/${GSK_CRYPT_PACKAGE} && \
    wget -O /tmp/${GSK_SSL_PACKAGE}  ${TSM_DOWNLOAD_URL}/${GSK_SSL_PACKAGE}    && \
    yum -y localinstall /tmp/*.rpm && \
    rm -fd /tmp/*.rpm

# Kubernetes does not allow mounting config maps without overriding 
# the entire "bin" folder - instead we inject symbolic links (c.f. entrypoint)
# but we need write access for any user there.
RUN chmod 777 /opt/tivoli/tsm/client/ba/bin

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
