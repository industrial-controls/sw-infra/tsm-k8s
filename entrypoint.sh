#!/bin/bash


if [[ -z "${DSM_CONFIG_DIR}" ]]; then
  DSM_CONFIG_DIR=/etc/dsm
fi

TSM_BIN_DIR=/opt/tivoli/tsm/client/ba/bin
if [ ! -z $USE_CONFIG_MAP ] && [ $USE_CONFIG_MAP = 'Y' ] && [ "$(ls -A $DSM_CONFIG_DIR)" ]; then
    for f in `ls $DSM_CONFIG_DIR`; do
        echo Mapping DSM Config Files to $TSM_BIN_DIR/$f
        ln -s $DSM_CONFIG_DIR/$f $TSM_BIN_DIR/$f
    done;
fi

echo Using password : ${TSM_PASSWORD}
dsmc i -password="${TSM_PASSWORD}"
