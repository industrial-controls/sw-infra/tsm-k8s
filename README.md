# TSM For Kubernetes (tsm-k8s)

IBM Tivoli TSM client for Kubernetes environments.

## TSM Setup

Once your CERN TSM node and password has been assigned, you will have to change the TSM password manually.
Create your ```dsm.sys``` and ```dsm.opt``` files and mount them as instructed below into a Docker container.

Running the docker image with :

    docker run -it --rm -e USE_CONFIG_MAP=Y -e DSM_CONFIG_DIR=/etc/dsm -v your/local/dsm.sys:/etc/dsm/dsm.sys -v your/local/dsm.opt:/etc/dsm/dsm.opt -v your/folder/to/backup:your/folder/to/backup/mounted gitlab-registry.cern.ch/industrial-controls/sw-infra/tsm-k8s:baseline

Then call the command :

    dsmc set password

to initialize your new password.

## Usage in OpenShift

In OpenShift, use the provided tsm-cronjob.yml to add a new CRON job to your project :

    oc process -f tsm-cronjob.yml -p NODE_NAME_PARAM=YOUR_NODE_NAME -p TSM_PASSWORD_PARAM=YOUR_TSM_PASSWORD | oc create -f -


To remove all TSM related elements from your project, execute :

    oc delete all,configmap,pvc,secret,job,cronjob --selector template=tsm
    
    
